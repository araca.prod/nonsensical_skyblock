#!/bin/sh
rm -rf utils/updlog
mkdir utils/updlog
mkdir utils/updlog/mods
{ {
	exec 9<utils/updatelist
	counter=0
	while read name repo <&9; do
		{ {
			while ! {
				#if [ "$CLEANUPD" = "" ] && ! [ "$(git ls-remote "$repo" | grep HEAD | cut -f 1)" = "$(head -n 1 utils/updlog/mods/"$name" | cut -d' ' -f 2)" ]; then
					rm -rf mods/"$name" && \
					git clone --depth 1 "$repo" mods/"$name" && \
					cd mods/"$name" && \
					git submodule init && \
					git submodule update && \
					git log -1 && \
					cd - >/dev/null && echo "SUCCESS($name)";
				#fi
			}; do echo FAILURE TO DOWNLOAD MOD "$name" 1>&2; done
		} | tee /dev/fd/4 2>&1; } > utils/updlog/mods/"$name" &
		counter=$(($counter+1))
		if [ "$counter" = $(nproc) ]; then
			wait
			counter=0
		fi
	done
	wait
	rm -rf $(find mods -name .git)
	rm -rf mods/zzzz_glcraft_crafthook
	mv mods/glcraft/zzzz_glcraft_crafthook mods/
	git add -A
} 2>&1; } 4>&2
